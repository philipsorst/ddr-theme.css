import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AlertsComponent} from './components/alerts.component';
import {IndexComponent} from './index.component';
import {NavsComponent} from './components/navs.component';
import {BreadcrumbComponent} from './components/breadcrumb.component';
import {PaginationComponent} from './components/pagination.component';
import {DefaultPageComponent} from './pages/default-page.component';
import {ButtonsComponent} from './components/buttons.component';
import {ComponentsComponent} from './components/components.component';
import {PageHeaderComponent} from './components/page-header.component';
import {FormsComponent} from './components/forms.component';
import {FormsModule} from '@angular/forms';
import {CardComponent} from "./components/card.component";

@NgModule({
    declarations: [
        AppComponent,
        ComponentsComponent,
        AlertsComponent,
        BreadcrumbComponent,
        ButtonsComponent,
        NavsComponent,
        PaginationComponent,
        PageHeaderComponent,
        DefaultPageComponent,
        IndexComponent,
        FormsComponent,
        CardComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule
{
}
