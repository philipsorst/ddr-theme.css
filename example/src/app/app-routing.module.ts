import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AlertsComponent} from './components/alerts.component';
import {IndexComponent} from './index.component';
import {NavsComponent} from './components/navs.component';
import {BreadcrumbComponent} from './components/breadcrumb.component';
import {PaginationComponent} from './components/pagination.component';
import {DefaultPageComponent} from './pages/default-page.component';
import {ButtonsComponent} from './components/buttons.component';
import {ComponentsComponent} from './components/components.component';
import {PageHeaderComponent} from './components/page-header.component';
import {FormsComponent} from './components/forms.component';
import {CardComponent} from "./components/card.component";


const routes: Routes = [
    {
        path: '',
        component: IndexComponent
    },
    {
        path: 'components',
        component: ComponentsComponent,
        children: [
            {
                path: 'alerts',
                component: AlertsComponent
            },
            {
                path: 'breadcrumb',
                component: BreadcrumbComponent
            },
            {
                path: 'buttons',
                component: ButtonsComponent
            },
            {
                path: 'navs',
                component: NavsComponent
            },
            {
                path: 'pagination',
                component: PaginationComponent
            },
            {
                path: 'page-header',
                component: PageHeaderComponent
            },
            {
                path: 'forms',
                component: FormsComponent
            },
            {
                path: 'card',
                component: CardComponent
            }
        ]
    },
    {
        path: 'pages/default',
        component: DefaultPageComponent
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule
{
}
